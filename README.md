# Запуск DHCP сервера в докер контейнере

## Настройка сервера
На машине, где будет разворачиваться контейнер, обязательно нужно установить ip адрес сетевому интерфейсу, к которому будем подключать устройства получающие от него IP адреса. Очень важно, чтобы он находился в том же диапазоне, что и адреса, которые будут через него раздваться.

Пример:
Будем раздавать адреса в диапазоне 198.168.1.2-198.168.1.255
Подключаем устройства в сетевой интерфейс eth0, следовательно устанавливаем eth0 ip: 198.168.1.1
После установки ip перезагружаем машину.

## docker-compose.yml
``` bash
version: "3.3"

services:
    dhcp:
        build: .
        container_name: dhcp
        tty: true
        ports:
            - "67:67/udp"
#           - "68:68/udp"
        restart: unless-stopped
        network_mode: 'host'
```

## Dockerfile
``` bash
FROM ubuntu:latest
USER root
RUN apt-get update && apt-get upgrade -y
RUN apt-get install isc-dhcp-server -y
RUN apt-get install rsyslog -y
COPY dhcpd.conf /etc/dhcp/dhcpd.conf
COPY isc-dhcp-server /etc/default/isc-dhcp-server
COPY start-dhcp.sh /etc/dhcp
RUN chmod +x /etc/dhcp/start-dhcp.sh

ENTRYPOINT ["/etc/dhcp/start-dhcp.sh"]
```

## start-dhcp.sh
``` bash
#!/bin/bash
/etc/init.d/isc-dhcp-server start
bash
```

## isc-dhcp-server
``` bash
INTERFACESv4=eth0
```

## dhcpd.conf

<details>
<summary>Тык!</summary>

``` bash
#chmod 644 dhcpd.conf
#
# Sample configuration file for ISC dhcpd for Debian
#
# Attention: If /etc/ltsp/dhcpd.conf exists, that will be used as
# configuration file instead of this file.
#
#

# The ddns-updates-style parameter controls whether or not the server will
# attempt to do a DNS update when a lease is confirmed. We default to the
# behavior of the version 2 packages ('none', since DHCP v2 didn't
# have support for DDNS.)
ddns-update-style none;

# option definitions common to all supported networks...
option domain-name "bnct.dhcp";
#option domain-name-servers ns1.example.org, ns2.example.org;

default-lease-time 86400;
max-lease-time 86400;

# If this DHCP server is the official DHCP server for the local
# network, the authoritative directive should be uncommented.
authoritative;

# Use this to send dhcp log messages to a different log file (you also
# have to hack syslog.conf to complete the redirection).
log-facility local7;

# No service will be given on this subnet, but declaring it helps the 
# DHCP server to understand the network topology.

#subnet 10.152.187.0 netmask 255.255.255.0 {
#}

# This is a very basic subnet declaration.

#subnet 192.168.0.0 netmask 255.255.255.0 {
#  range 192.168.1.196 192.168.1.200;
#}

subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.191 192.168.1.195;
}



# This declaration allows BOOTP clients to get dynamic addresses,
# which we don't really recommend.

#subnet 10.254.239.32 netmask 255.255.255.224 {
#  range dynamic-bootp 10.254.239.40 10.254.239.60;
#  option broadcast-address 10.254.239.31;
#  option routers rtr-239-32-1.example.org;
#}

# A slightly different configuration for an internal subnet.
#subnet 10.5.5.0 netmask 255.255.255.224 {
#  range 10.5.5.26 10.5.5.30;
#  option domain-name-servers ns1.internal.example.org;
#  option domain-name "internal.example.org";
#  option subnet-mask 255.255.255.224;
#  option routers 10.5.5.1;
#  option broadcast-address 10.5.5.31;
#  default-lease-time 600;
#  max-lease-time 7200;
#}

# Hosts which require special configuration options can be listed in
# host statements.   If no address is specified, the address will be
# allocated dynamically (if possible), but the host-specific information
# will still come from the host declaration.

#host passacaglia {
#  hardware ethernet 0:0:c0:5d:bd:95;
#  filename "vmunix.passacaglia";
#  server-name "toccata.fugue.com";
#}

# Fixed IP addresses can also be specified for hosts.   These addresses
# should not also be listed as being available for dynamic assignment.
# Hosts for which fixed IP addresses have been specified can boot using
# BOOTP or DHCP.   Hosts for which no fixed address is specified can only
# be booted with DHCP, unless there is an address range on the subnet
# to which a BOOTP client is connected which has the dynamic-bootp flag
# set.
#host fantasia {
#  hardware ethernet 08:00:07:26:c0:a5;
#  fixed-address fantasia.fugue.com;
#}

# You can declare a class of clients and then do address allocation
# based on that.   The example below shows a case where all clients
# in a certain class get addresses on the 10.17.224/24 subnet, and all
# other clients get addresses on the 10.0.29/24 subnet.

#class "foo" {
#  match if substring (option vendor-class-identifier, 0, 4) = "SUNW";
#}

#shared-network 224-29 {
#  subnet 10.17.224.0 netmask 255.255.255.0 {
#    option routers rtr-224.example.org;
#  }
#  subnet 10.0.29.0 netmask 255.255.255.0 {
#    option routers rtr-29.example.org;
#  }
#  pool {
#    allow members of "foo";
#    range 10.17.224.10 10.17.224.250;
#  }
#  pool {
#    deny members of "foo";
#    range 10.0.29.10 10.0.29.230;
#  }
#}


#-------#
# Autom #
#-------#


#-------#
# Video #
#-------#

host Lan02CameraRadminBunker {
  
}

host Lan03CameraRadminControlRoom {
  
}

host WireScanner {
  
}

host BNCTBLACKM425DN {
  
}

host BNCTCOLORHPM553 {
  
}

host Printer3D {
  
}
```

</details>

## Запуск

Все файлы выше должны лежать в одной директории на сервере (ну либо нужно будет редактировать пути в Dockerfile).

Стартуем:
``` bash
sudo docker-compose build && sudo docker-compose up -d
```

После старта проверяем, что все хорошо:
``` bash
sudo docker logs dhcp
```

Должны получить вывод:
``` bash
Launching IPv4 server only.
    * Starting ISC DHCPv4 server dhcpd              [ OK ]
```
